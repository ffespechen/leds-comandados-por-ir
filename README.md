# LEDS comandados por IR

Nueve LEDS que pueden encenderse a voluntad, utilizando un control remoto infrarrojo, utilizando Arduino NANO.

Se usa la librería Arduino-IRemote, que puede descargarse de https://github.com/z3t0/Arduino-IRremote

Material y componentes empleados:

01 (uno) Arduino NANO V3

01 (uno) Sensor infrarrojo genérico (montado en una plaqueta)

01 (uno) Control remoto infrarrojo genérico

09 (nueve) LEDs de diferentes colores, en el ejemplo tres grupos de tres

09 (nueve) Resistencias para colocar en serie con los LEDs, en el ejemplo de 1KOhm

Cables M-M varios
