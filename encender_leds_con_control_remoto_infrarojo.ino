#include <IRremote.h>

//Receptor Infrarrojo
int ReceptorIR =3;
IRrecv irrecv(ReceptorIR);
decode_results Codigo;

//LEDs
const int pinInicio = 4;
const int pinFinal = 12;


void setup()
{
  irrecv.enableIRIn();

  //Inicializo los pines para los LEDs
  for(int j=pinInicio; j<= pinFinal; j++)
  {
    pinMode(j, OUTPUT);
  }
}


//Funciones auxiliares para encender y apagar los leds

void encenderLeds(int cantidadLeds)
{
  for(int i=pinInicio; i<(pinInicio+cantidadLeds);i++)
  {
    digitalWrite(i, HIGH);
  }
}

void apagarLeds()
{
  for(int i=pinInicio; i<=pinFinal;i++)
  {
    digitalWrite(i, LOW);
  }  
}


void loop()
   {
    if (irrecv.decode(&Codigo))
        {

        apagarLeds();

        switch (Codigo.value)
        {
          case 16738455:
          encenderLeds(1);
          break;

          case 16750695:
          encenderLeds(2);
          break;  

          case 16756815:
          encenderLeds(3);
          break;

          case 16724175:
          encenderLeds(4);
          break;

          case 16718055:
          encenderLeds(5);
          break;

          case 16743045:
          encenderLeds(6);
          break;

          case 16716015:
          encenderLeds(7);
          break;

          case 16726215:
          encenderLeds(8);
          break;

          case 16734885:
          encenderLeds(9);
          break;

          case 16730805:
          break;

          default:
          break;  
          }

        delay(500);
        irrecv.resume();
        }
   }
