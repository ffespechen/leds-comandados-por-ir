const int pinInicio = 4;
const int pinFinal = 12;

void setup() {

  for(int j=pinInicio; j<= pinFinal; j++)
  {
    pinMode(j, OUTPUT);
  }

}

void encenderLeds(int cantidadLeds)
{
  for(int i=pinInicio; i<(pinInicio+cantidadLeds);i++)
  {
    digitalWrite(i, HIGH);
  }
}

void apagarLeds()
{
  for(int i=pinInicio; i<=pinFinal;i++)
  {
    digitalWrite(i, LOW);
  }  
}

void loop() {

  for(int k=1; k<=9; k++)
  {
    apagarLeds();
    encenderLeds(k);
    delay(2000);
  }

}
